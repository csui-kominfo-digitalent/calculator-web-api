package id.ac.ui.cs.digitalent.handson.calcws.model;

import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class OutputTest {

    @Test
    public void insert_shouldAddKeyValue() throws Exception {
        Output test = new Output(0L, 1L, new HashMap<>());

        assertNull(test.insert("foo", "bar"));
        assertEquals("bar", test.insert("foo", "bar"));
    }

    @Test
    public void getElapsedTime_shouldReturnResultCorrectly() throws Exception {
        Output test = new Output(100L, 200L, Collections.EMPTY_MAP);

        assertEquals(100L, test.getElapsedTime().longValue());
    }
}