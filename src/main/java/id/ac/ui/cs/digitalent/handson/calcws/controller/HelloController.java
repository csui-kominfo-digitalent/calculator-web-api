package id.ac.ui.cs.digitalent.handson.calcws.controller;

import id.ac.ui.cs.digitalent.handson.calcws.model.Output;
import id.ac.ui.cs.digitalent.handson.calcws.util.ResponseHelper;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Log
@RestController
public class HelloController {

    private static final String MESSAGE = "Hello, World";
    private static final Integer IMPORTANT_NUMBER = 42;

    @GetMapping("/hello")
    public Output greeting() {
        Instant start = Instant.now();
        Map<String, Object> payload = new HashMap<>();

        payload.put("message", MESSAGE);
        payload.put("answer", IMPORTANT_NUMBER);

        Output output = ResponseHelper.prepareOutput(start, payload);
        log.fine(output.toString());

        return output;
    }
}
