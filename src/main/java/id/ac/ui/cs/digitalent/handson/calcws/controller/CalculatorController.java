package id.ac.ui.cs.digitalent.handson.calcws.controller;

import id.ac.ui.cs.digitalent.handson.calcws.model.Output;
import id.ac.ui.cs.digitalent.handson.calcws.util.ResponseHelper;

import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Log
@RequestMapping("/calculator")
@RestController
public class CalculatorController {

    @GetMapping("/is_even/{number}")
    public Output isEven(@PathVariable("number") Integer number) {
        Instant start = Instant.now();
        Map<String, Object> payload = new HashMap<>();

        Boolean isEven = number % 2 == 0 ? Boolean.TRUE : Boolean.FALSE;
        payload.put("result", isEven.toString());

        Output output = ResponseHelper.prepareOutput(start, payload);
        log.fine(output.toString());

        return output;
    }

    @GetMapping("/is_odd/{number}")
    public Output isOdd(@PathVariable("number") Integer number) {
        Instant start = Instant.now();
        Map<String, Object> payload = new HashMap<>();

        Boolean isOdd = number % 2 != 0 ? Boolean.TRUE : Boolean.FALSE;
        payload.put("result", isOdd.toString());

        Output output = ResponseHelper.prepareOutput(start, payload);
        log.fine(output.toString());

        return output;
    }

    @GetMapping("/rectangle_area/{width}/{height}")
    public Output computeRectangleArea(@PathVariable Integer width,
                                       @PathVariable Integer height) {
        Instant start = Instant.now();
        Map<String, Object> payload = new HashMap<>();

        Integer area = width * height;
        payload.put("result", area);

        Output output = ResponseHelper.prepareOutput(start, payload);
        log.fine(output.toString());

        return output;
    }

    @GetMapping("/fibonacci/{number}")
    public Output computeFibonacci(@PathVariable Integer number) {
        Instant start = Instant.now();
        Map<String, Object> payload = new HashMap<>();

        Integer result = getFibonacciRecursive(number);
        payload.put("result", result);

        Output output = ResponseHelper.prepareOutput(start, payload);
        log.fine(output.toString());

        return output;
    }

    private Integer getFibonacciRecursive(Integer number) {
        if (number <= 2) {
            return 1;
        }

        return getFibonacciRecursive(number - 1) +
                getFibonacciRecursive(number - 2);
    }
}
