# Calculator Web API
> Spring Boot-based Web service example for demonstrating Web app container
> autoscaling on Azure

[![pipeline status](https://gitlab.com/csui-kominfo-digitalent/calculator-web-api/badges/master/pipeline.svg)](https://gitlab.com/csui-kominfo-digitalent/calculator-web-api/commits/master)

## Getting Started

Kebutuhan-kebutuhan minimal untuk dapat menjalankan dan mengembangkan contoh
ini lebih lanjut adalah sebagai berikut:

- Java JDK (v8 atau lebih tinggi)
- Gradle (v4.10.2 atau lebih tinggi)
- Teks editor atau IDE favorit Anda

Untuk menjalankan Web service ini secara lokal, panggil perintah berikut di
terminal/*shell* (*command prompt*) favorit Anda:

```bash
$ gradlew bootRun
```

Contoh Web service akan aktif dan dapat diakses melalui *host* `localhost`
dengan *port* `8080`. Silakan buka peramban (*browser*) Anda dan masukkan
tautan (URL) berikut untuk mencoba beberapa fitur yang tersedia di Web
service:

- [Hello, World `http://localhost:8080/hello`](http://localhost:8080/hello)
- [Cek apakah sebuah angka adalah bilangan genap `http://localhost:8080/calculator/is_even/8`](http://localhost:8080/calculator/is_even/8)
- [Cek apakah sebuah angka adalah bilangan odd `http://localhost:8080/calculator/is_odd/9`](http://localhost:8080/calculator/is_even/8)
- [Hitung bilangan Fibonacci ke-sekian `http://localhost:8080/calculator/fibonacci/10`](http://localhost:8080/calculator/fibonacci/10)

Web service akan mengembalikan data berupa JSON yang dapat dilihat di layar
*browser* Anda.

Untuk mematikan Web service lokal, tekan CTRL-C pada *shell* aktif Anda.

## Hands-On: Autoscaling di Microsoft Azure

Beban kerja suatu sistem yang operasional pada layanan komputasi awan (*cloud
computing*) dapat bervariasi dalam suatu rentang waktu. Pada suatu waktu, bisa
saja tidak ada atau sedikit sekali pengguna yang mengakses sistem. Namun ada
juga waktu dimana jumlah pengguna yang mengakses sistem secara konkuren sangat
banyak sehingga membuat waktu respon sistem menjadi semakin lambat.

Kita dapat mengatur ukuran dan jumlah sumber daya komputasi yang digunakan
oleh sistem yang sedang operasional di *cloud*. Terdapat dua pendekatan dalam
hal ini, yaitu **vertical scaling** dan **horizontal scaling**.

*Vertical scaling* adalah mengatur **ukuran** sumber daya komputasi. Salah satu
contohnya adalah menambah jumlah RAM atau CPU pada VM di *cloud*. Sedangkan
*Horizontal scaling* adalah mengatur **jumlah** sumber daya komputasi. Contohnya
adalah memperbanyak jumlah *application server* yang menjalankan sebuah aplikasi
Web.

Dalam *hands-on* ini, kita akan melihat contoh bagaimana Microsoft Azure dapat
melakukan *autoscaling* secara **horizontal** ketika beban kerja sebuah Web
service melebihi batasan tertentu dalam suatu periode.

### 1. Buat App Services

Buka portal Azure, lalu pilih untuk membuat *resource* baru bertipe
**App Services**. Kemudian cari pilihan untuk membuat **Web App for
Containers**. Pada layar selanjutnya, silakan isi bebas nama aplikasi
dan *resource group*. Namun pastikan **OS** diatur ke **Linux**.

### 2. Mengatur Container

Selanjutnya, klik pilihan **Configure Containers**. Contoh Web service sudah
disiapkan dalam format *container* Docker dan akan di-deploy ke *instance*
Azure Web App milik Anda. Pilih *tab* **Single Container (Preview)**, lalu pilih
opsi **Docker Hub**. Masukkan `addianto/web-calculator-api:latest` di *form*
**Image and optional tag** dan pastikan **Repository Access** diatur ke **Public**.
Klik tombol **Apply**. Lalu di tampilan konfigurasi **App Services**, klik **Create**.
Tunggu beberapa saat hingga dapat notifikasi bahwa *resource* baru selesai
dibuat.

> Catatan: apabila Anda memiliki contoh aplikasi Web/Web service berbasiskan
> *framework* Spring Boot dalam format *container* Docker dan sudah di-publish
> di suatu *container registry*, Anda dapat menggunakannya di sini.

### 3. Mengatur Aplikasi (Web Service)

Lakukan navigasi ke **Service Plan** yang dibuatkan oleh Azure. **Service Plan**
tersebut akan mengandung aplikasi Web (Web Service) yang di-deploy menggunakan
*container* Docker. Klik nama aplikasi Web Anda, lalu tampilannya akan berganti
ke konfigurasi **App Service**. Di dalam tampilan **App Service**, masuk ke
**Application Settings** dan tambahkan sebuah **App Setting Name** baru.
Tambahkan **App Setting Name** berjudul `PORT` dengan nilai `8080`.

Contoh hasil akhir dari konfigurasi aplikasi di langkah ini dapat dilihat pada
cuplikan gambar berikut:

![Contoh tampilan App Settings](docs/app_settings.png)

### 4. Mengatur Autoscaling

Masih di tampilan **App Service**, cari dan pilih opsi **Scale out (App Service
plan)**. Tampilan awal akan memunculkan *slider* yang menentukan jumlah mesin
(*instance*) yang menjalankan aplikasi Web Anda. Anda dapat mengatur sendiri
jumlahnya secara manual atau menerapkan **autoscale** yang akan melakukan
*scaling* secara otomatis berdasarkan kondisi-kondisi yang Anda buat.

Untuk menyalakan *autoscale*, klik **Enable autoscale**. Anda akan diberikan
sebuah *default scale condition* yang dapat Anda isi sendiri dengan aturan
(*rule*) sesuai keinginan Anda. Silakan eksplorasi opsi-opsi yang tersedia
dalam pembuatan aturan-aturan dalam *default scale condition*.

Gambar cuplikan berikut adalah contoh *scale condition* dengan rincian:

- Jumlah mesin (*instance*) akan bertambah 1 buah apabila rata-rata penggunaan
CPU lebih dari 70% berdurasi 10 menit
- Jumlah mesin (*instance*) akan berkurang 1 buah apabila rata-rata penggunaan
CPU kurang dari 25% berdurasi 10 menit 

![Contoh tampilan konfigurasi autoscale](docs/autoscale.png)

### 5. Simulasi Autoscale dengan Stress Test

Anda perlu membuat Web service Anda sibuk menangani banyak *requests* agar
dapat memicu kondisi aktivasi *autoscale*. Cara paling sederhana adalah
dengan membuka banyak *tab* di *browser* dan setiap *tab* membuka alamat
Web service perhitungan bilangan Fibonacci dengan parameter bilangan cukup
besar (misal: 40 s.d. 45).

Salah satu *tool* untuk secara sengaja melakukan *stress test* pada Web
service dapat dilihat di *snippets* $1784226. Silakan disesuaikan URL di dalam
kode agar menunjuk ke URL Web service perhitungan Fibonacci Anda.

## Maintainers

- [@addianto](https://gitlab.com/addianto)

## License

[MIT](LICENSE) (c) Daya Adianto
